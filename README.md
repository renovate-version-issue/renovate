# Renovate Bot  

Keep dependencies up to date to protect from library vulnerabilities

See the [Renovate docs](https://docs.renovatebot.com/getting-started/running/#docker-image) for more info  


# How it works  

Renovate Bot will scan repositories in the `autodiscover-filter` flag.  

It will raise a merge request for any library upgrade requires.  

Run a [schedule manually]() to trigger Renovate, and check the pipeline of scanned app  
