module.exports = {
  endpoint: 'http://gitlab.com/api/v4/',
  platform: 'gitlab',
  persistRepoData: true,
  packageRules: [
    {
      groupName: "all non-major dependencies",
      groupSlug: "all-minor-patch",
      matchPackagePatterns: [
        "*"
      ],
      matchUpdateTypes: [
        "minor",
        "patch"
      ]
    }
  ]
};
