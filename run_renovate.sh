#!/bin/sh
set -e

# For available flags, see: https://docs.renovatebot.com/self-hosted-configuration/
# autodiscover-filter uses minimatch glob-style pattern, you can test out changes in https://globster.xyz/
RENOVATE_EXTRA_FLAGS=$1

renovate \
    --autodiscover=true \
    --onboarding=false \
    --config-migration=true \
    --require-config=ignored \
    --automerge=true \
    --automerge-type=pr \
    --platform-automerge=true \
    --autodiscover-filter="renovate-version-issue/renovate-graphql-version-issue" \
    $RENOVATE_EXTRA_FLAGS
